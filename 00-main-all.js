console.log('Step 1');
console.log('--------------------------------------');
(function () {
  let price = 5;
  let quantity = 2
  let total = price * quantity;
  
  console.log(`total is ${total}`);
  
  price = 20
  
  console.log(`total is ${total}`);
})()


// we need to save total in storage and be able to rerun it whenever price or quantity is changed

console.log('');
console.log('Step 2');
console.log('--------------------------------------');
(function () {
  let price = 5;
  let quantity = 2
  let total = 0
  let target = null
  let storage = []

  target = () => { total = price * quantity };

  // add the code to the storage
  function record() { storage.push(target) };
  function replay() { storage.forEach(run => run())};

  record()
  replay()
  
  console.log(`total is ${total}`);
  price = 20
  console.log(`total is ${total}`);
  replay()
  console.log(`total is ${total}`);
})()

// to create a more scalable solution let's add a class to store our dependencies

console.log('');
console.log('Step 4 - Adding class to store dependencies');
console.log('--- Observer design patter ---');
console.log('--------------------------------------');
(function () {
  class Dep {
    constructor() {
      this.subscribers = []
    }

    // depend functions replaces our record function from earlier
    // if there is a target & we don't already have it
    depend() {
      if (target && !this.subscribers.includes(target)) {
        this.subscribers.push(target);
      }
    }

    // run all our subscribers
    // vue also has a dep class )
    notify() {
      this.subscribers.forEach(sub => sub());
    }
  }

  // create a new Dep instance
  const dep = new Dep();

  let price = 5;
  let quantity = 2
  let total = 0

  let target = () => { total = price * quantity };

  dep.depend();
  target();
  
  console.log(`total is ${total}`);
  price = 20
  console.log(`total is ${total}`);
  dep.notify();
  console.log(`total is ${total}`);
})()

// soon we will have a Dep instance for each variable. How can we encapsulate the code that needs to be watched / recorded?
// we want to have watcher function like watch(() => { total = price * quantity}) instead of 
// let target = () = {
//   total = price * quantity;
// };
// dep.depend();
// target();

console.log('');
console.log('Step 5 - Adding watcher');
console.log('--------------------------------------');
(function () {
  class Dep {
    constructor() {
      this.subscribers = []
    }

    // depend functions replaces our record function from earlier
    // if there is a target & we don't already have it
    depend() {
      if (target && !this.subscribers.includes(target)) {
        this.subscribers.push(target);
      }
    }

    // run all our subscribers
    // vue also has a dep class )
    notify() {
      this.subscribers.forEach(sub => sub());
    }
  }

  // create a new Dep instance
  const dep = new Dep();
  let price = 5;
  let quantity = 2;
  let total = 0;
  let target = null;

  function watcher(myFunc) {
    target = myFunc;
    dep.depend();
    target()
    target = null
  }
  

  watcher(() => { total = price * quantity });
  
  console.log(`total is ${total}`);
  price = 20
  console.log(`total is ${total}`);
  dep.notify();
  console.log(`total is ${total}`);
})()


// we want each of our variables to have its own Dep instance, so when it's updated it can re-run all the functions it needs to
// data is now in properties let data = { price: 5, quantity: 2 }

console.log('');
console.log('Step 9 - Putting it all together');
console.log('--------------------------------------');
(function() {
  let data = { price: 5, quantity: 2 };
  let target, total, salePrice;
  
  class Dep {
    constructor() {
      this.subscribers = [];
    }
    depend() {
      if (target && !this.subscribers.includes(target)) {
        this.subscribers.push(target)
      }
    }
    notify() {
      this.subscribers.forEach(sub => sub())
    }
  }
  
  Object.keys(data).forEach(key => {
    let internalValue = data[key];
  
    const dep = new Dep();
  
    Object.defineProperty(data, key, {
      get() {
        dep.depend() // <-- Remember the target we're running
        return internalValue;
      },
      set(newVal) {
        internalValue = newVal;
        dep.notify() // <-- Rerun saved tagrets
      }
    })
  })
  
  function watcher(myFunc) {
    target = myFunc;
    target();
    target = null;
  }
  
  watcher(() => {
    total = data.price * data.quantity;
  })
  
  watcher(() => {
    salePrice = data.price * 0.9;
  })
})();

