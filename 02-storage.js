// we need to save total in storage and be able to rerun it whenever price or quantity is changed

console.log('');
console.log('Step 2');
console.log('--------------------------------------');

let price = 5;
let quantity = 2
let total = 0
let target = null
let storage = []

target = () => { total = price * quantity };

// add the code to the storage
function record() { storage.push(target) };
function replay() { storage.forEach(run => run())};

record()
replay()

console.log(`total is ${total}`);
price = 20
console.log(`total is ${total}`);
replay()
console.log(`total is ${total}`);