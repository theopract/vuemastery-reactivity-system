class Dep {
  constructor() {
    this.subscribers = []
  }

  // depend functions replaces our record function from earlier
  // if there is a target & we don't already have it
  depend() {
    if (target && !this.subscribers.includes(target)) {
      this.subscribers.push(target);
    }
  }

  // run all our subscribers
  // vue also has a dep class )
  notify() {
    this.subscribers.forEach(sub => sub());
  }
}

// create a new Dep instance
const dep = new Dep();

let price = 5;
let quantity = 2
let total = 0

let target = () => { total = price * quantity };

dep.depend();
target();