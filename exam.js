let data = { price: 5, quantity: 2 };
let target = null;
let total = 0;

class Dep {
  constructor() {
    this.subscribers = []
  }
  depend() {
    if (target && !this.subscribers.includes(target)) {
      this.subscribers.push(target);
    }
  }
  notify() {
    this.subscribers.forEach(dep => dep());
  }
}

Object.keys(data).forEach(key => {
  let internalValue = data[key];

  const dep = new Dep();

  Object.defineProperty(data, key, {
    get() {
      dep.depend();
      return internalValue;
    },
    set(newValue) {
      internalValue = newValue;
      dep.notify();
    }
  })

})

function watcher(myFunc) {
  target = myFunc;
  target();
  target = null;
}

watcher(() => { total = data.price * data.quantity });
